const bcrypt = require ('bcrypt');

//Return encrypted string
function hash (data){
    console.log("Hasing data");
    // devolvemos un strig con data encriptado
    return bcrypt.hashSync(data, 10); //hasSync para hacerlo de forma síncrona para esperar a que acabe el proceso de encriptación.
}

//Función para validar el password. Devuelve true/false
function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
    console.log("Checking password");
   
    return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
   }

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;