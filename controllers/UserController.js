
const io = require ('../io');
const requestJson = require ('request-json');
const crypt = require ('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujis10ed/collections/"
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
console.log (mLabAPIKey);

function getUsersV1 (req, res) {
   console.log("GET /apitechu/v1/users");

   var result = {};
   var users = require('../usuarios.json');

   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length;   }

   result.users = req.query.$top ?
     users.slice(0, req.query.$top) : users;

   res.send(result);
 }

 function getUsersV2 (req, res) {
   console.log("GET /apitechu/v2/users");

   var httpClient = requestJson.createClient(baseMLabURL); //Creamos el cliente http
   console.log("Client Created");

   httpClient.get("user?" + mLabAPIKey,
      function(err, resMLAb, body) { //En resMLAb está la respuesta del servidor en RAW
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
          }
          //console.log(resMLAb); //Para analizar si hay problemas en la consulta.
          res.send(response);
      }

   )
 }

 function getUserByIdV2(req, res) {
   console.log ("GET /apitechu/v2/users/:id");

   var httpClient = requestJson.createClient(baseMLabURL);
   console.log("Client Created");

   var id = req.params.id; //Leemos el id del usuario de la petición 
   var query = 'q={"id":' + id+ '}';
   console.log ("Consulta: " + baseMLabURL + "user?"+ query + "&" + mLabAPIKey);
   httpClient.get("user?"+ query + "&" + mLabAPIKey,
      function(err, resMLAb, body) {
        if(err){  // escribimos errores en base a lo que puede pasar y devolvemos el codigo de status http que corresponde
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
              var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send (response);
      }     
  )
 }





function createUserV1 (req, res) {
    console.log("POST /apitechu/v1/users");

    //console.log(req.headers);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser ={
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email
    }

    var users = require('../usuarios.json');
    users.push(newUser); //añadimos el usuario al array de usuarios que tenemos en memoria
    io.writeUserDatatoFile(users);
    console.log("Usuario añadido con éxito");
    res.send({"msg" : "Usuario añadido con éxito"});
}

function createUserV2 (req, res) {
  console.log("POST /apitechu/v2/users");
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser ={
      "id" : req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
  }
  
  var httpClient = requestJson.createClient(baseMLabURL); //Creamos el cliente http
   console.log("Client Created");

   httpClient.post("user?" + mLabAPIKey, newUser, //el segundo parámetro el el body que se envía en el post. En este caso el nuevo usuario newUser
      function(err, resMLAb, body) { //En resMLAb está la respuesta del servidor en RAW
        console.log("Usuario creado en MLab");
        res.status(201).send({"msg":"Usuario guardardo"});
      }
   )
}

function deleteUserV1 (req, res) {
   console.log("DELETE /apitechu/v1/users/:id");
   console.log("id es " + req.params.id);

   var users = require('../usuarios.json');
   var deleted = false;

   // console.log("Usando for normal");
   // for (var i = 0; i < users.length; i++) {
   //   console.log("comparando " + users[i].id + " y " +  req.params.id);
   //   if (users[i].id == req.params.id) {
   //     console.log("La posicion " + i + " coincide");
   //     users.splice(i, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for in");
   // for (arrayId in users) {
   //   console.log("comparando " + users[arrayId].id + " y "  + req.params.id);
   //   if (users[arrayId].id == req.params.id) {
   //     console.log("La posicion " + arrayId " coincide");
   //     users.splice(arrayId, 1);
   //     deleted = true;
   //     break;
   //   }
   // }

   // console.log("Usando for of");
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición ? coincide");
   //     // Which one to delete? order is not guaranteed...
   //     deleted = false;
   //     break;
   //   }
   // }

    console.log("Usando for of 2");
   // Destructuring, nodeJS v6+
    users.entries(); //-> la función entries devuelve el índice y el objeto del array y los mete en [index, user]
    for (var [index, user] of users.entries()) {
      console.log("comparando " + user.id + " y " +  req.params.id);
      if (user.id == req.params.id) {
        console.log("La posicion " + index + " coincide");
        users.splice(index, 1);
        deleted = true;
        break;
      }
    }

   // console.log("Usando for of 3");
   // var index = 0;
   // for (user of users) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posición " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //     break;
   //   }
   //   index++;
   // }

   // console.log("Usando Array ForEach");
   //  Usar forEach para los casos en los que tenga que recorrer una lista entera porque no tiene break.
   //  Debe usarse en los casos en los que haya que hacer una transformación en todos los elementos, pero no
   //  para buscar.
   // users.forEach(function (user, index) {
   //   console.log("comparando " + user.id + " y " +  req.params.id);
   //   if (user.id == req.params.id) {
   //     console.log("La posicion " + index + " coincide");
   //     users.splice(index, 1);
   //     deleted = true;
   //   }
   // });

   // console.log("Usando Array findIndex");
   // Busco el índice del elemento
   // var indexOfElement = users.findIndex(
   //   function(element){
   //     console.log("comparando " + element.id + " y " +   req.params.id);
   //     return element.id == req.params.id
   //   }
   // )
   //
   // Hago lo que necesite (borrar) con el elemento
   // console.log("indexOfElement es " + indexOfElement);
   // if (indexOfElement > 0) {
   //   users.splice(indexOfElement, 1);
   //   deleted = true;
   // }

   if (deleted) {
     io.writeUserDatatoFile(users);
   }

   var msg = deleted ? "Usuario borrado" : "Usuario no encontrado."

   console.log(msg);
   res.send({"msg" : msg});
 }



 //Exponemos las funciones para que se puedan usar desde fuera de UserController.js
 //Buena práctica, ponerlas en el mismo orden en el que aparecen en el fichero.
 module.exports.getUsersV1 = getUsersV1;
 module.exports.getUsersV2 = getUsersV2;
 module.exports.getUserByIdV2 = getUserByIdV2;
 module.exports.createUserV1 = createUserV1;
 module.exports.createUserV2 = createUserV2;
 module.exports.deleteUserV1 = deleteUserV1;

