const io = require ('../io');
const requestJson = require ('request-json');
const crypt = require ('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujis10ed/collections/"
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV1 (req, res) {
    console.log("POST /apitechu/v1/login");
    console.log("Email: " + req.body.email);
    console.log("Password: " + req.body.password);
    
    var users = require('../usuarios.json');
    var logged = false;
    for (var i = 0; i < users.length; i++) {
        console.log("comparando " + users[i].email + " y " + req.body.email + "  " + users[i].password + "  " + req.body.password);
           if (users[i].email == req.body.email && users[i].password == req.body.password) {
               console.log("******La posicion " + i + " coincide******");
               users[i].logged = true;
               logged = true;
               console.log(users[i]);
               console.log("Usuario actualizado con éxito -> logged = true agregado al usuario de id: " + users[i].id);
               break;
            }
         
        } 
        if (logged){
            io.writeUserDatatoFile(users);
            res.send ({"mensaje":"Login correcto", "idUsuario": users[i].id});
        } else {
            res.send({"mensaje" : "Login incorrecto"});
        }
    }
// MI CÓDIGO PARA loginV2
// function loginV2 (req, res){
//     console.log("POST /apitechu/v2/login");
//     console.log(req.body.email);
//     console.log(req.body.password);

//     var httpClient = requestJson.createClient(baseMLabURL);
//     console.log("Client Created");

//     var email = req.body.email; //Leemos el email del usuario de la petición 
//     var query = 'q={"email":"' + email + '"}';
//     console.log ("Consulta: " + baseMLabURL + "user?"+ query + "&" + mLabAPIKey);
//     httpClient.get("user?"+ query + "&" + mLabAPIKey,
//       function(getErr, getResMLAb, getBody) {
//           console.log(getBody);
//         if(getErr){  // escribimos errores en base a lo que puede pasar y devolvemos el codigo de status http que corresponde
//           var response = {
//             "msg" : "Error obteniendo usuario"
//           }
//           res.status(500);
//         } else {
//           if (getBody.length > 0) {
//               var pwdPlain = req.body.password;
//               var pwdCrypted = getBody[0].password;
//             console.log ("pwd plain: " + pwdPlain + " pwdCrypted: " + pwdCrypted);
//               if (crypt.checkPassword(pwdPlain, pwdCrypted)){
//                   console.log("Contraseña correcta");
//                   // AHORA ESCRIBIMOS EL CAMPO logged:true en el usuario con un PUT
//                   var idUsuario = getBody[0].id;
//                   console.log (idUsuario);
//                   var putBody = '{"$set":{"logged":true}}'; 
//                   httpClient.put ("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
//                     function(putErr, putResMLAb, putBody) {
//                         console.log("función PUT. Usuario actualizado con logged:true");
//                         var response = {
//                             "msg":"Login correcto",
//                             "idUsuario":getBody[0].id
//                         };
//                     }
//                   )
//               }
              
//           } else {
//             var response = {
//               "msg" : "Usuario no encontrado"
//             }
//             res.status(404);
//           }
//         }
//         res.send (response);
//       }     
//   )

// }


function logoutV1 (req, res){
    console.log("POST /apitechu/v1/logout/:id");
    console.log("id es: "+req.params.id);
    var users = require('../usuarios.json');
    //Busco el usuario
    var logout = false;
    for (var i = 0; i < users.length; i++) {
        console.log("comparando " + users[i].id + " y " +  req.params.id);
           if (users[i].id == req.params.id && users[i].logged) {
               console.log("El usuario con email " + users[i].email + "tiene la propiededad Logged a: " + users[i].logged);
               delete users[i].logged;
               logout = true; 
               console.log("Logged borrado, ahora tiene que ser undefined -> "+ users[i].logged);
               break;
            }
        }
        if (logout){
            io.writeUserDatatoFile(users);
            res.send ({"mensaje":"logout correcto", "idUsuario ": users[i].id})
        } else {
            res.send ({"mensaje":"logout incorrecto"});
        }
    }

// MI CÓDIGO PARA logoutV2
// function logoutV2 (req, res) {
//     console.log ("POST /apitechu/v2/logout/:id");
//     console.log ("id param: " + req.params.id);
    
//     var httpClient = requestJson.createClient(baseMLabURL);
//     console.log("Client Created");
//     var idUsuario = req.params.id; //Leemos el email del usuario de la petición 
//     var query = 'q={"id":' + idUsuario + '}'; //Qué registro quiero actualizar
//     console.log ("Consulta: " + baseMLabURL + "user?"+ query + "&" + mLabAPIKey);
//     var logout = false;
//     httpClient.get("user?"+ query + "&" + mLabAPIKey,
//     function(getErr, getResMLAb, getBody) {
//         console.log(getBody);
//       if(getErr){  // escribimos errores en base a lo que puede pasar y devolvemos el codigo de status http que corresponde
//         var response = {
//           "msg" : "Error obteniendo usuario"
//         }
//         res.status(500);
//       } else {
//         if (getBody.length > 0) {
//             if (getBody[0].id == req.params.id && getBody[0].logged){
//                 console.log ("dentro del if. getBody[0].id:" + getBody[0].id + " y getBody[0].logged: " + getBody[0].logged)
//                 console.log("Usuario correcto y existe el atributo logged");
//                 // AHORA BORRAMOS EL CAMPO logged:true en el usuario con un PUT
//                 var idUsuario = getBody[0].id;
//                 console.log (idUsuario);
//                 logout = true;
//                 var query = 'q={"id":' + idUsuario + '}'; //Qué registro quiero actualizar
//                 var putBody = '{"$unset":{"logged":""}}' //Qué quiero actualizar del recurso
//                 httpClient.put ("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
//                   function(putErr, putResMLAb, putBody) {
//                       console.log("función PUT. Usuario actualizado borrando logged:true");
//                     //   var response = {
//                     //       "msg":"Logout correcto",
//                     //       "idUsuario":getBody[0].id
//                     //   };
//                   }
//                 )
//                 console.log ("Estado de logout: " + logout);   
//             }
//         } 
//         if (logout) {
//             var response = {"mensaje":"logout correcto", "idUsuario ": getBody[0].id}; 
//         } else {
//             var response = {"msg" : "logout incorrecto"};
//           }
//       }
//       res.send (response);
//     }  
//   )
// }

//CÓDIGO DEL PROFESOR
//LOGIN V2
function loginV2(req, res) {
    console.log("POST /apitechu/v2/login");
   
    var query = 'q={"email": "' + req.body.email + '"}';
    console.log("query es " + query);
   
    httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
   
        var isPasswordcorrect =
          crypt.checkPassword(req.body.password, body[0].password);
        console.log("Password correct is " + isPasswordcorrect);
   
        if (!isPasswordcorrect) {
          var response = {
            "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
          }
          res.status(401);
          res.send(response);
        } else {
          console.log("Got a user with that email and password, logging in");
          query = 'q={"id" : ' + body[0].id +'}';
          console.log("Query for put is " + query);
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              var response = {
                "msg" : "Usuario logado con éxito",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          )
        }
      }
    );
   }

//CÓDIGO DEL PROFESOR LOGOUT V2
function logoutV2(req, res) {
    console.log("POST /apitechu/v2/logout/:id");
   
    var query = 'q={"id": ' + req.params.id + '}';
    console.log("query es " + query);
   
    httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        if (body.length == 0) {
          var response = {
            "mensaje" : "Logout incorrecto, usuario no encontrado"
          }
          res.send(response);
        } else {
          console.log("Got a user with that id, logging out");
          query = 'q={"id" : ' + body[0].id +'}';
          console.log("Query for put is " + query);
          var putBody = '{"$unset":{"logged":""}}'
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
              console.log("PUT done");
              var response = {
                "msg" : "Usuario deslogado",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          )
        }
      }
    );
   }


module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;