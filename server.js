require('dotenv').config(); //libería para usar las variables de entorno. con config() carga los valores del fichero de variables.


const express = require('express');
const app = express();


const port = process.env.PORT || 3000; //El valor por defecto es 3000 si no hay ya una variable que lo haya fijado a otro número

// Código para que funcione el CORS en el servidor y podamos atacar al API desde el front (que está en un dominio diferente)
var enableCORS = function(req, res, next) {
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
    res.set("Access-Control-Allow-Headers", "Content-Type");
   
    next();
   }

app.use(express.json()); //Le decimos a Express que preprocese los bodys de las peticiones asumiendo
                        // que tiene formato json. Si no llega un json fallará

app.use(enableCORS); //Activamos CORS (Cross Domain) para que las peticines del front nos lleguen al API


const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');


app.listen(port);

console.log("API escuchando en el puerto: "+ port);
/*
app.get('/apitechu/v1/hello', //Definimos la url del API
    function(req, res) { //Registramos la URL y el recurso
        console.log("GET /apitechu/v1/hello");
        res.send({"msg" : "Hola desde API TechU"}); //Devolvemos un json directmente
    }
)*/

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);

app.post('/apitechu/v1/users', userController.createUserV1);
app.post('/apitechu/v2/users', userController.createUserV2);

app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v2/login', authController.loginV2);

app.post('/apitechu/v1/logout/:id', authController.logoutV1);
app.post('/apitechu/v2/logout/:id', authController.logoutV2);


app.post("/apitechu/v1/monstruo/:p1/:p2",  //Sólo responde a una llamada a /monstruo/ con dos parámetros/:p1/:p2
    function (req, res) {
        console.log("Parámetros");
        console.log(req.params);

        console.log ("Query String");
        console.log (req.query);

        console.log ("Headers");
        console.log (req.headers);

        console.log ("Body");
        console.log (req.body);

    }
)

/*
app.delete("/apitechu/v1/users/:id",
    function (req, res) {
        console.log("DELETE_nuevo /apitechu/v1/users/:id/");
        console.log("id: " + req.params.id);
        var users = require ('./usuarios.json');
        var deleted = false;
        /**PRIMERA FORMA DE RECORRER EL ARRAY */
        // console.log ("Usando for normal");
        /*for (i=0; i<users.length;i++) {
            console.log ("Comparando " + users[i].id + " y " + req.params.id);
            console.log ("i="+i);
            //console.log (users[i].id +",");
            if (users[i].id == req.params.id){
                console.log ("La posición " + i + " coincide");
                users.splice(i, 1);
                deleted = true;
                console.log ("Usuario " + i + " borrado de la lista")
                break;
            }*/
        /**SEGUNDA FORMA DE RECORRER EL ARRAY */
        /*users.forEach(function(element, index) {
            console.log("usuario " + index + " es " + element.id);
            if (element.id == req.params.id){
                users.splice(index, 1);
                console.log("Usuario " + element.id + " borrado de la lista");
            }
          });*/
        
        /** FOR IN, versión del profesor
        console.log ("Usando for in");
        for (arrayID in users) { //Itera sobre las propiedades enumerables de un objeto
            console.log("Comparando " + users[arrayId].id + " y " + req.params.id);
            if (users[arrayId].id == req.params.id) {
                console.log ("La posición " + arrayId + " coincide.");
                users.splice(arrayId, 1);
                deleted = true;
                break;
            } 
        }

        /** TERCERA FORMA DE RECORRER EL ARRAY */
        /*for (i in users) {
            //console.log(users[i])
            if (users[i].id == req.params.id) {
                console.log("ID del usuario en el if " + users[i].id);
                users.splice(i,1);
            }
        }*/
        
        /** CUARTA FORMA DE RECORRER EL ARRAY */
        /*i=0;
        for (user of users) {
            console.log(user.id);
            if (user.id == req.params.id){
                console.log("Entro en el if");
                console.log(user.id);
                users.splice(i,1);
            }
            i++;
          }*/
        
        /** QUINTA FORMA DE RECORRER EL ARRAY 
        users.forEach(function (users, index) {
            console.log(index);
            console.log(users);
            if (users.id == req.params.id){

            }
        });
        
        if (deleted) {
            writeUserDatatoFile(users);
        }
        var msg = deleted ? "Usuario borrado" : "Usuario no encontrado."
        console.log(msg);
        res.send({"msg":msg});
    }
)*/