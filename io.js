const fs = require('fs');

function writeUserDatatoFile(data) {
    console.log("writeUserDatatoFile");
    var jsonUserData = JSON.stringify(data); //pasamos el array a formato JSON, que es lo que hay que escribir en el fichero
    
    //Escribimos en el fichero fs.writeFile(fichero, datos, enconding, funcion si error)
    fs.writeFile("./usuarios.json", jsonUserData, "utf8",
        function(err) {
            if (err) {
                console.log(err);
            } else {
                console.log("Usuario persistido");
            }
        } 
    )
}
module.exports.writeUserDatatoFile = writeUserDatatoFile;